package spellingbee.client.game;

import java.util.Random;

/**
 * Simple spelling bee game to test the gooey
 * 
 * @author Neil Fisher (1939668)
 */
public class SimpleSpellingBeeGame implements ISpellingBeeGame {

	private String letters = "";
	private int score = 0;

	public SimpleSpellingBeeGame() {
		this.letters = "AHIELVY";
	}

	private boolean isPanagram(String attempt) {
		if (attempt.length() >= 7) {
			for (int i = 0; i < getAllLetters().length(); i++) {
				if (attempt.indexOf(getAllLetters().charAt(i)) == -1)
					return false;
			}
			return true;
		}
		return false;
	}

	// not working perfectly, but this was fixed later in SpellingBeeGame.java
	private boolean isValid(String attempt) {
		for (int i = 0; i < attempt.length(); i++) {
			// checks if letter given is in the list of letters
			// (may not be needed later with working gooey)
			if (getAllLetters().indexOf(attempt.charAt(i)) == -1)
				return false;
		}
		// if it doesn't contain center letter, return false
		if (getAllLetters().indexOf(getCenterLetter()) == -1)
			return false;

		return true;
	}

	@Override
	public int getPointForWord(String attempt) {
		if (isValid(attempt) == false)
			return -1;

		int pointsEarned = 0;

		if (attempt.length() == 4)
			pointsEarned = 1;
		else if (attempt.length() > 4) {
			pointsEarned = attempt.length();
			if (isPanagram(attempt))
				pointsEarned += 7;
		}

		// words less than 4 letters => 0 points
		return pointsEarned;
	}

	@Override
	public String getMessage(String attempt) {
		int pointsEarned = getPointForWord(attempt);

		if (pointsEarned > 0)
			return (new Random().nextBoolean() ? "Good! " : "Great! ") + pointsEarned + " points earned.";

		if (pointsEarned == -1)
			return "Word invalid!";

		return "Less than 4 letters!";
	}

	@Override
	public String getAllLetters() {
		return letters;
	}

	@Override
	public char getCenterLetter() {
		return getAllLetters().charAt(getAllLetters().length() / 2);
	}

	@Override
	public int getScore() {
		return score;
	}

	@Override
	public int[] getBrackets() {
		int[] brackets = {4, 8, 12, 16, 20};
		return brackets;
	}
}
