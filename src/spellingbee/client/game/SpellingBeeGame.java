package spellingbee.client.game;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;

/**
 * @author Castiel Le (1933080)
 */
public class SpellingBeeGame implements ISpellingBeeGame {
	private String allLetters;
	private char centerLetter;
	private int score;
	private List<String> wordsFound = new ArrayList<String>();
	public HashSet<String> possibleWords;
	private static Random rand = new Random();
	private static final Path letterCombinations = Paths.get("Dictionary\\letterCombinations.txt");
	private static final Path english = Paths.get("Dictionary\\english.txt");
	
	/**
	 * @author Castiel Le (1933080)
	 * First constructor of SpellingBeeGame with random variable
	 */
	public SpellingBeeGame() {
		allLetters = getRandomLetters(letterCombinations);
		centerLetter = getRandomCenter(allLetters);
		possibleWords = getPossibleWords(english);
		score = getScore();
	}
	
	/**
	 * @author Castiel Le (1933080)
	 * @param letters the specific combination of 7 letters
	 * Second constructor of SpellingBeeGame with a specific combination as an input
	 */
	public SpellingBeeGame(String letters) {
		centerLetter = getRandomCenter(letters);
		possibleWords = getPossibleWords(english);
		score = getScore();
	}
	
	/**
	 * @author Castiel Le (1933080)
	 * Getter method for score
	 */
	@Override
	public int getScore() {
		return score;
	}
	
	/**
	 * @author Castiel Le (1933080)
	 * @param pointsEarned points user gets after 1 round
	 * Calculate the current total score
	 */
	public void addPoints(int pointsEarned) {
		score += pointsEarned;
	}
	
	/**
	 * @author Castiel Le (1933080)
	 * Getter method for allLetters
	 */
	@Override
	public String getAllLetters() {
		return allLetters;
	}

	/**
	 * @author Castiel Le (1933080)
	 * Getter method for centerLetter
	 */
	@Override
	public char getCenterLetter() {
		return centerLetter;
	}

	/**
	 * @author Neil Fisher (1939668)
	 * @author Castiel Le (1933080)
	 * @return int - represents the points earned for the attempt. Negative if attempt is invalid
	 */
	@Override
	public int getPointForWord(String attempt) {
		if (!isValid(attempt))
			return -1;

		int pointsEarned = 0;

		if (attempt.length() == 4)
			pointsEarned = 1;
		else if (attempt.length() > 4) {
			pointsEarned = attempt.length();
			if (isPanagram(attempt))
				pointsEarned += 7;
		}

		// update wordsFound array for additional feature in GameTab
		wordsFound.add(attempt);
		// update score variable
		addPoints(pointsEarned);

		// words less than 4 letters => 0 points
		return pointsEarned;
	}

	/**
	 * @author Neil Fisher (1939668)
	 * @return String - represents the message that comes from the number of points earned from the
	 *         user's attempt
	 */
	@Override
	public String getMessage(String attempt) {
		int pointsEarned = getPointForWord(attempt);

		if (pointsEarned > 0)
			return (rand.nextBoolean() ? "Good! " : "Great! ") + pointsEarned + " points earned.";

		if (pointsEarned == -1)
			return "Word invalid!";

		return "Less than 4 letters!";
	}

	/**
	 * @author Castiel Le (1933080)
	 * @return int[] array of threshold of the amount score the user can get from the combination
	 * Calculate 25%, 50%, 75%, 90% and 100% of the total score you can get from the combination and put them in an array
	 */
	@Override
	public int[] getBrackets() {
		int totalscore = 0;
		for (String s : possibleWords) {
			if (s.length() == 4) {
				totalscore += 1;
			}
			else if (s.length() > 4 && s.length() < 7) {
				totalscore += s.length();
			}
			else if (s.length() >= 7) {
				if (isPanagram(s)) {
					totalscore += (s.length() + 7);
				}
				else {
					totalscore += s.length();
				}
			}
		}
		int[] progress = new int[5];
		progress[0] = totalscore;
		progress[1] = (int) (totalscore * 0.9);
		progress[2] = (int) (totalscore * 0.75);
		progress[3] = (int) (totalscore * 0.5);
		progress[4] = (int) (totalscore * 0.25);
		return progress;
	}

	/**
	 * @author Castiel Le (1933080)
	 * @param word a word from all word can possible be created from the combination
	 * @return boolean true if the word is a panagram and false if it is not
	 * Check if the word is a panagram or not
	 */
	public boolean isPanagram(String word) {
		int check = 0;
		for (int i = 0; i < allLetters.length(); i++) {
			if (word.contains(Character.toString(allLetters.charAt(i)))) {
				check++;
			}
		}
		return check == 7;
	}
	
	/**
	 * @author Castiel Le (1933080)
	 * @param path path to the text file 
	 * @return List<String> array of every words from the text file
	 * Split the text file content into a List<String> array of words
	 */
	public static List<String> createWordsFromFile(String path) {
		Path dictionarypath = Paths.get(path);
		List<String> lines = new ArrayList<String>();
		try {
			lines = Files.readAllLines(dictionarypath);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return lines;
	}
	
	/**
	 * @author Castiel Le (1933080)
	 * @param letterpath path to the letterCombination.txt file
	 * @return String a random set of letters from the text file
	 * Get a random set of letters from the text file
	 */
	public String getRandomLetters(Path letterpath) {
		List<String> letter = new ArrayList<String>();
		String comb = null;
		try {
			letter = Files.readAllLines(letterpath);
			comb = letter.get(rand.nextInt(letter.size()));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return comb;
	}
	
	/**
	 * @author Castiel Le(1933080)
	 * @param word the combination of letters
	 * @return char a random letter from the combination
	 * Get a random letter from the combination as the center letter
	 */
	public char getRandomCenter(String word) {
		return word.charAt(rand.nextInt(7));
	}
	
	/**
	 * @author Castiel Le(1933080)
	 * @param possiblewordpath path to the english.txt file
	 * @return HasSet<String> an set of all possible words can be created from the combination in the text file
	 * Get a list of all the word than can be created with the combination in the english.txt file
	 */
	public HashSet<String> getPossibleWords(Path possiblewordpath) {
		HashSet<String> possible = new HashSet<String>();
		List<String> wholeDictionary = new ArrayList<String>();
		try {
			wholeDictionary = Files.readAllLines(possiblewordpath);
			for (String word : wholeDictionary) {
				if (testWordPossible(word)) {
					possible.add(word);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return possible;
	}

	/**
	 * @author Neil Fisher (1939668)
	 * @author Castiel Le (1933080)
	 * @return boolean - represents the validity of the attempt depending on certain criteria
	 */
	public boolean testWordPossible(String word) {
		// greater than 3 letters and has center letter
		if (word.length() >= 4 && word.contains(Character.toString(centerLetter))) {
			for (int i = 0; i < word.length(); i++) {
				// checks if all the letters in the attempt are supplied
				if (!(allLetters.contains(Character.toString(word.charAt(i))))) {
					return false;
				}
			}
			return true;
		}
		else {
			return false;
		}
	}
	
	/**
	 * @author Neil Fisher (1939668)
	 * @param attempt the input from user
	 * @return boolean true if the word satisfies the given condition of the game and false if it does not
	 * Check if the word meets the condition of the game
	 */
	private boolean isValid(String attempt) {
		if (testWordPossible(attempt) == false || wordsFound.contains(attempt) || !possibleWords.contains(attempt))
			return false;
		return true;
	}
}
