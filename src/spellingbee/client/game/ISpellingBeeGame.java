package spellingbee.client.game;

/**
 * 
 * @author Castiel Le (1933080)
 * @author Neil Fisher (1939668)
 * Interface for SpellingBeeGame, giving the must have methods
 * 
 */
public interface ISpellingBeeGame {
	public int getPointForWord(String attempt);
	public String getMessage(String attempt);
	public String getAllLetters();
	public char getCenterLetter();
	public int getScore();
	public int[] getBrackets();
}
