package spellingbee.client.gui;

import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.TabPane;
import javafx.scene.control.TabPane.TabClosingPolicy;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import spellingbee.client.Client;

/**
 * @author Neil Fisher (1939668)
 */
public class SpellingBeeClient extends Application {
	private Client client = new Client();

	/**
	 * standard code for making a javafx application. a GameTab and a ScoreTab are added to a Tabpane
	 */
	@Override
	public void start(Stage stage) throws Exception {
		stage.setTitle("Spelling Bee (Neil Fisher & Castiel Le)");

		Group root = new Group();

		Scene scene = new Scene(root, 300, 350);
		scene.setFill(Color.BLACK);
		// Tabs
		TabPane tabPane = new TabPane();
		GameTab gameTab = new GameTab(client);
		tabPane.getTabs().add(gameTab);
		ScoreTab scoreTab = new ScoreTab(client);
		tabPane.getTabs().add(scoreTab);
		tabPane.setTabClosingPolicy(TabClosingPolicy.UNAVAILABLE);

		// gametab score event listener
		TextField scoreField = gameTab.getScoreField();
		scoreField.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				scoreTab.refresh();
			}
		});

		root.getChildren().add(tabPane);
		stage.setMinWidth(315);
		stage.setMinHeight(175);
		stage.setOpacity(0.9);
		stage.setScene(scene);
		stage.show();
	}

	public static void main(String[] args) {
		Application.launch(args);
	}
}