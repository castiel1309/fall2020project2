package spellingbee.client.gui;

import java.util.ArrayList;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.Tab;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import spellingbee.client.Client;

/**
 * GameTab is added to tabPane in the javafx application This could be more organized by putting
 * event listeners in a different class, but our goal wasn't to make it look great
 * 
 * @author Neil Fisher
 */
public class GameTab extends Tab {
	private TextField scoreField;

	/**
	 * GameTab constructor. Creates the javafx nodes that are necessary for the application to work
	 * 
	 * @param client - the client object that is used program wide
	 */
	public GameTab(Client client) {
		super("Game"); // superclass constuctor -> tab name "Game"
		// this.client = client;

		// creating nodes
		VBox Vnodes = new VBox();

		// textfield guess
		// TextFields need to be placed before buttons because of their event listeners
		TextField guess = new TextField();

		// letter buttons
		HBox letterButtonsContainer = new HBox();
		ArrayList<Button> letterButtons = new ArrayList<Button>();
		String letters = client.sendAndWaitMessage("getAllLetters");
		char centerLetter = client.sendAndWaitMessage("getCenterLetter").charAt(0);
		for (int i = 0; i < letters.length(); i++) {
			// get all letters, separate them and put them in separate buttons
			Button btn = new Button(Character.toString(letters.charAt(i)));
			if (letters.charAt(i) == centerLetter)
				btn.setTextFill(Color.RED);
			// event listeners
			btn.setOnAction(new EventHandler<ActionEvent>() {
				@Override
				public void handle(ActionEvent e) {
					guess.appendText(btn.getText());
				}
			});
			letterButtons.add(btn);
		}
		letterButtonsContainer.getChildren().addAll(letterButtons);

		// info (message, score)
		HBox info = new HBox();
		TextField message = new TextField();
		message.setText("Spelling Bee");
		message.setEditable(false);
		scoreField = new TextField();
		scoreField.setText("Score: 0");
		scoreField.setEditable(false);
		info.getChildren().addAll(message, scoreField);

		// shows all guesses at bottom of viewport
		TextArea guessList = new TextArea("Guesses: \n");
		guessList.setEditable(false);
		guessList.setMaxWidth(300);
		guessList.setPrefRowCount(6);

		// control buttons (message, score)
		HBox controlButtons = new HBox();
		Button submit = new Button("Submit");
		Button clear = new Button("Clear");
		controlButtons.getChildren().addAll(submit, clear);
		// event listeners
		submit.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				if (!guess.getText().trim().isEmpty()) {
					String[] response = client.sendAndWaitMessage("getMessage;" + guess.getText().trim()).split(";");
					message.setText(response[0]);
					scoreField.setText("Score: " + response[1]);

					String textBefore = guessList.getText();
					guessList.setText(textBefore + guess.getText() + "\n");
					guess.setText("");
				}
			}
		});
		clear.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				guess.setText("");
			}
		});

		Vnodes.getChildren().addAll(letterButtonsContainer, guess, controlButtons, info, guessList);
		// done creating nodes

		// set the content of the Tab
		this.setContent(Vnodes);
	}

	/**
	 * @return scoreField - TextField that represents the Score displayed in a gametab object
	 */
	public TextField getScoreField() {
		return scoreField;
	}

}
