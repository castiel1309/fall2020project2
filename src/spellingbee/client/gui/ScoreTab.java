package spellingbee.client.gui;

import java.util.ArrayList;

import javafx.scene.control.Tab;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import spellingbee.client.Client;

/**
 * 
 * @author Castiel Le (1933080)
 * 
 * This is the javafx ScoreTab shows the score of the player 
 * and what level the player's at base on the total point can possibly be created with the combination
 *
 */
public class ScoreTab extends Tab {
	private Client client;
	private ArrayList<Text> brackets = new ArrayList<Text>();
	private ArrayList<Text> bracketvalues = new ArrayList<Text>();
	private String[] thresholdResponse;
	/**
	 * @author Castiel Le (1933080)
	 * @param client Client object that is used program wide
	 * Create all the field for the ScoreTab GUI
	 */
	public ScoreTab(Client client) {
		super("Score");
		this.client = client;
		//get the threshold score from the server
		thresholdResponse = this.client.sendAndWaitMessage("getBrackets").split(";");
		//adding the first column of the grid to an ArrayList<String>
		brackets.add(new Text("Queen Bee"));
		brackets.add(new Text("Professional"));
		brackets.add(new Text("Amateur"));
		brackets.add(new Text("Rookie"));
		brackets.add(new Text("Newbie"));
		brackets.add(new Text("Score"));
		//adding the second column of the grid to another ArrayList<String>
		for (int i = 0; i < brackets.size() - 1; i++) {
			bracketvalues.add(new Text());
			bracketvalues.get(i).setText(thresholdResponse[i]);
		}
		bracketvalues.add(new Text("0"));
		//add color to the fields
		for (int i = 0; i < brackets.size() - 1; i++) {
			brackets.get(i).setFill(Color.GREY);
			bracketvalues.get(i).setFill(Color.WHITE);
		}
		//separate the player and player's score with the levels and threshold score with different colors
		brackets.get(brackets.size() - 1).setFill(Color.WHITE);
		bracketvalues.get(bracketvalues.size() - 1).setFill(Color.RED);
		GridPane scoreTab = new GridPane();
		//add the fields to the grid
		scoreTab.add(brackets.get(0), 0, 0);
		scoreTab.add(brackets.get(1), 0, 1);
		scoreTab.add(brackets.get(2), 0, 2);
		scoreTab.add(brackets.get(3), 0, 3);
		scoreTab.add(brackets.get(4), 0, 4);
		scoreTab.add(brackets.get(5), 0, 5);
		scoreTab.add(bracketvalues.get(0), 1, 0);
		scoreTab.add(bracketvalues.get(1), 1, 1);
		scoreTab.add(bracketvalues.get(2), 1, 2);
		scoreTab.add(bracketvalues.get(3), 1, 3);
		scoreTab.add(bracketvalues.get(4), 1, 4);
		scoreTab.add(bracketvalues.get(5), 1, 5);
		//spacing the column for better appearance 
		scoreTab.setHgap(10);
		//add the node to GUi
		this.setContent(scoreTab);
	}
	/**
	 * @author Castiel Le (1933080)
	 * refresh the score tab everytime they create a new valid word 
	 * and check if the score reaches to any levels so the levels' name can be change color accordingly 
	 */
	public void refresh() {
		String scoreResponse = client.sendAndWaitMessage("getScore");
		bracketvalues.get(bracketvalues.size() - 1).setText(scoreResponse);
		for(int i = 0; i < thresholdResponse.length; i++) {
			if(Integer.parseInt(scoreResponse) > Integer.parseInt(thresholdResponse[i])) {
				brackets.get(i).setFill(Color.WHITE);
			}
		}
	}
}
