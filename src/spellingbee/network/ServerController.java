package spellingbee.network;

import spellingbee.client.game.ISpellingBeeGame;
import spellingbee.client.game.SpellingBeeGame;

/**
 * This class will run on the server side and is used to connect the server code to the backend
 * business code. This class is where the "protocol" will be defined.
 */
public class ServerController {
	// This is the interface you will be creating!
	// Since we are storing the object as an interface, you can use any type of
	// ISpellingBeeGame object.
	// This means you can work with either the SpellingBeeGame OR
	// SimpleSpellingBeeGame objects and can
	// seamlessly change between the two.
	// private ISpellingBeeGame spellingBee = new SpellingBeeGame("EAHILVY".toLowerCase());
	private ISpellingBeeGame spellingBee = new SpellingBeeGame();

	/**
	 * Action is the method where the protocol translation takes place. This method is called every
	 * single time that the client sends a request to the server. It takes as input a String which is
	 * the request coming from the client. It then does some actions on the server (using the
	 * ISpellingBeeGame object) and returns a String representing the message to send to the client
	 * 
	 * @param  inputLine The String from the client
	 * @return           The String to return to the client
	 */
	public String action(String inputLine) {
		/*
		 * Your code goes here!!!! Note: If you want to preserve information between method calls, then you
		 * MUST store it into private fields. You should use the spellingBee object to make various calls
		 * and here is where your communication code/protocol should go. For example, based on the samples
		 * in the assignment: if (inputLine.equals("getCenter")) { // client has requested getCenter. Call
		 * the getCenter method which returns a String of 7 letters return spellingBee.getCenter(); } else
		 * if ( ..... )
		 */

		// public int getPointForWord(String attempt);
		// public String getMessage(String attempt);
		// public String getAllLetters();
		// public char getCenterLetter();
		// public int getScore();
		// public int[] getBrackets();

		// paramaters are separated by ;
		// ex: getMessage;alive -> getMessage("alive")
		// when an array is returned, entries are separated by ';'
		String[] actions = inputLine.split(";");
		if (actions[0].equals("getCenterLetter")) {
			return Character.toString(spellingBee.getCenterLetter());
		}
		else if (actions[0].equals("getAllLetters")) {
			return spellingBee.getAllLetters();
		}
		else if (actions[0].equals("getMessage")) {
			// after ';'
			return spellingBee.getMessage(actions[1]) + ";" + spellingBee.getScore();
		}
		else if (actions[0].equals("getBrackets")) {
			int[] brackets = spellingBee.getBrackets();
			String array = "";
			for (int bracket : brackets)
				array += bracket + ";";
			return array;
		}
		else if (actions[0].equals("getScore")) {
			return spellingBee.getScore() + "";
		}
		return null;
	}
}
